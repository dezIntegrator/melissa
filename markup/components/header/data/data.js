var data = {
    header:{
        ru: {
            callToAction: 'Записаться на бесплатный бета-тест',
            btnText: 'Отправить',
            lang: 'ru'
        },

        en:{
            callToAction: 'Sign up for a free beta test',
            btnText: 'Send',
            lang: 'en'
        },
    }
};
