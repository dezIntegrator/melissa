var data= {
    hero:{
        ru: {
            title: '«Melissa»',
            description: 'Самый ожидаемый продукт для QA специалистов',
            features: [
                {
                    feature: 'Регрессионное тестирование верстки',
                    needDivider: true
                },
                {
                    feature: 'Проверка сайта на наличие битых ссылок'
                }
            ]
        },

        en: {
            title: '«Melissa»',
            description: 'Самый ожидаемый продукт для QA специалистов',
            features: [
                {
                    feature: 'Регрессионное тестирование верстки',
                    needDivider: true
                },
                {
                    feature: 'Проверка сайта на наличие битых ссылок'
                }
            ]
        }
    }
};
